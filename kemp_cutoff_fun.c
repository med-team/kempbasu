/*
 * Calculate the optimal significance level cutoff
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <glib.h>

#include "cutoff_fun.h"
#include "util.h"
#include "matrix.h"
#include "compat.h"

#define ELT gsl_matrix_get

char *opt_pars_file=NULL;

// Linear log cutoff function
double log_cutoff_function_linear(cutoff_fun_t *pars,short k,double lx) {
  gsl_matrix *p=pars->coeffs;
  //printf("%lf %lf\n",ELT(p,k-2,3),ELT(p,k-2,4));
  //printf("%lf %lf\n",ELT(p,k-3,3),ELT(p,k-3,4));
  return ELT(p,k-2,3)+ELT(p,k-2,4)*lx;
}

// Quadratic log cutoff function
double log_cutoff_function_quadratic(cutoff_fun_t *pars,short k,double lx) {
  gsl_matrix *p=pars->coeffs;
  return ELT(p,k-2,0)*lx*lx+ELT(p,k-2,1)*lx+ELT(p,k-2,2);
}

double cutoff_function(cutoff_fun_t *pars,short k,unsigned int x,
		       double *alpha) {

  if (x==0)
    return 0.0;

  k=MIN(k,pars->max_k);

  double lx=log1(x);

  double log_alpha_linear=log_cutoff_function_linear(pars,k,lx);
  double log_alpha_quadratic=log_cutoff_function_quadratic(pars,k,lx);
  double a=pars->patch_region[0];
  double b=pars->patch_region[1];

  //printf("Interval [%lf,%lf]\n",a,b);

  //printf("lalpha1=%lf lapha2=%lf\n",log_alpha_linear,log_alpha_quadratic);

  // Quadratic region
  if (x <= a) {
    *alpha=exp(log_alpha_quadratic);
    //printf("Quadritic form: %lf\n",*alpha);
    return *alpha;
  }
  // Linear region
  if (x >= b) {
    *alpha=exp(log_alpha_linear);
    //printf("Linear form: %lf\n",*alpha);
    return *alpha;
  }
  // Patch region
  double gamma=(x-a)/(b-a);
  //printf("gamma = %lf lalpha1=%lf lapha2=%lf\n",gamma,log_alpha_linear,log_alpha_quadratic);
  *alpha = exp(gamma * log_alpha_linear + (1.0 - gamma) * log_alpha_quadratic);
  return *alpha;
}

cutoff_fun_t *read_cutoff_function(int argc,char **argv) {
  cutoff_fun_t *cutoff_fun=(cutoff_fun_t*)malloc(sizeof(cutoff_fun_t));

  if (cutoff_fun==NULL) {
    fprintf(stderr,"Failed to allocate memory");
    exit(-1);
  }

  void *coeffs;
  char *params_filename;
  FILE *params;

  if (opt_pars_file) {
    params_filename=opt_pars_file;
  } else {
    params_filename=find_file(argc,argv,"kemp.pars");
  }

  printf("# Reading cutoff parameters file '%s'\n",params_filename);

  if ( (params=fopen(params_filename,"r")) == NULL ) {
    fprintf(stderr,"Can't open kemp parameters file '%s'\n",params_filename);
    exit(-1);
  }
  fscanf(params,"%lf",&(cutoff_fun->patch_region[0]));
  fscanf(params,"%lf",&(cutoff_fun->patch_region[1]));

  printf("# Patch region [%f,%f]\n",
	 cutoff_fun->patch_region[0],
	 cutoff_fun->patch_region[1]);

  matrix_read(params,MAT_DOUBLE,&coeffs);
  //matrix_print(stdout,MAT_DOUBLE,coeffs);

  cutoff_fun->coeffs=(gsl_matrix *)coeffs;
  cutoff_fun->max_k = 1+cutoff_fun->coeffs->size1;
  printf("# Coefficients: k_max = %i\n",cutoff_fun->max_k);
  fflush(stdout);

  fclose(params);
  g_free(params_filename);
  return cutoff_fun;
}

