#include "util.h"

#include <stdlib.h>
#include <glib.h>
#include <stdio.h>

char *try_home(char *name) {
  const char *home=g_get_home_dir();
  char *file=g_build_filename(home,".kempbasu",name,NULL);

  printf("Trying '%s'\n",file);
  if (g_file_test(file,G_FILE_TEST_EXISTS))
    return file;
  else {
    g_free(file);
    return NULL;
  }
}

char *try_cwd(char *name) {
  char *cwd=g_get_current_dir();
  char *file=g_build_filename(cwd,name,NULL);

  printf("Trying '%s'\n",file);
  if (g_file_test(file,G_FILE_TEST_EXISTS))
    return file;
  else {
    g_free(file);
    return NULL;
  }
}

char *try_bindir(char *bin,char *name) {
  char *bindir=g_path_get_dirname(bin);
  char *file=g_build_filename(bindir,name,NULL);

  printf("Trying '%s'\n",file);
  if (g_file_test(file,G_FILE_TEST_EXISTS))
    return file;
  else {
    g_free(file);
    return NULL;
  }
}

char *try_etc(char *etcdir,char *name) {
  char *file=g_build_filename(etcdir,name,NULL);

  printf("Trying '%s'\n",file);
  if (g_file_test(file,G_FILE_TEST_EXISTS))
    return file;
  else {
    g_free(file);
    return NULL;
  }
}


#define TRY_PATH(try,name) \
if( (path= try(name)) != NULL) \
  return path;

char *find_file(int argc,char **argv,char *name) {
  char *path=NULL;

  // if( (path=try_home(name)) != NULL)
  //  return path;

  TRY_PATH(try_home,name);
  TRY_PATH(try_cwd,name);

  if( (path=try_etc("/etc/",name)) != NULL)
    return path;

  if( (path=try_etc("/usr/local/etc/",name)) != NULL)
    return path;
}
