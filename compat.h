#ifndef _KEMPBASU_COMPAT_H
#define _KEMPBASU_COMPAT_H

#ifdef _MSC_VER
#  define inline _inline
#  define isnan _isnan
#  define log1(x)   log(double(x))
#else
# define log1 log
#endif

#endif
