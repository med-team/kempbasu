#!/usr/bin/env ruby


require 'tempfile'

def nprocs
  if File.exists? "/proc/cpuinfo"
    output=`cat /proc/cpuinfo | grep "^processor" | wc -l`
    nprocs=output.to_i
  else
    nprocs=2
  end
  return nprocs
end

prgname=File.basename(__FILE__)
dirname=File.dirname(__FILE__)
if ARGV.length < 1
  puts "Missing input matrix filename"
  exit -1
end

inputname=ARGV[0]
basename=File.basename(File.basename(inputname,".dat"),".txt");

resultname=basename + "-#{prgname}.txt"
logname=basename + ".log"

$stderr.puts "Running #{prgname}"
$stderr.puts "  directory #{dirname}"
$stderr.puts "  reading #{inputname}"

out=$stdout

File.open(inputname,"r") do |file|
  header=file.readline
  samples=header.split("\t").length-1
  line_count=0
  file.each {|line| line_count+=1}

  file.rewind

  out.puts "#{line_count} #{samples} "
  header=file.readline
  file.each do |line|
    lst=line.split("\t")
    lst.shift
    out.print lst.join("\t")
    #print ">",lst.join("\t")
  end
end

