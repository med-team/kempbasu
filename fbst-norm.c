// Copyright (C) 2007 Leonardo Varuzza <varuzza@gmail.com>
//
// This program is free software; you can redistribute it and/or modify it
// under the term of the GNU Lesser General Public License as published by the
// Free Software Foundation; either version 2 of the License, or (at your
// option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License
// for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program; if not, write to the Free Software Foundation,
// Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
// _________________

#include <stdlib.h>
#include <gsl/gsl_math.h>
#include <gsl/gsl_sf_psi.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_min.h>
#include <gsl/gsl_randist.h>
#include <gsl/gsl_blas.h>

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <math.h>
#include "kempbasu.h"

#define ELT gsl_vector_uint_get

#define DUAL_INTEGRATE

FBSTConfig _FBST_DEFAULTS={NULL};
FBSTConfig *FBST_DEFAULTS=&_FBST_DEFAULTS;

inline double square(double x) {
  return x*x;
}

/*
 * log likelihood = Sum x_i ( log(N_i) + log(p_i) ) - ( Sum x_i) * log((Sum N_i * p_i))
 */



typedef struct {
  double cutoff;
  double likenorm;
  gsl_vector *means;
  gsl_vector *vars;
  unsigned int k;
  size_t dim;
} Params;

// Log of target Probability
static double prob_fun (double *x, size_t dim, void *_params) {
  Params *params=(Params*)_params;
  double prod=1.0;
  size_t i;

  //a*(log(Na)+log(k[0]))+b*(log(Nb)+log(k[1]))+(-a-b)*log(Na*k[0]+Nb*k[1]);

  for(i=0;i<dim;i++) {
    double mu=gsl_vector_get(params->means,i);
    double sigma2=gsl_vector_get(params->vars,i);
    double prob = gsl_ran_gaussian_pdf(x[i]-mu,sigma2);
    prod*=prob;
  }
  return prod;
}

// Density over H0
static double prob_H0_fun (double x, void *_params) {
  Params *params=(Params*)_params;
  size_t dim=params->dim;
  size_t i;

  double prod=1.0;

  for(i=0;i<dim;i++) {
    double  mu=gsl_vector_get(params->means,i);
    double sigma2=gsl_vector_get(params->vars,i);
    double prob = gsl_ran_gaussian_pdf(x-mu,sigma2);
    //printf("N(%lf,%lf)=%lf\n",mu,sigma2,prob);
    prod*=prob;
  }
  //printf("\n%lf\n",prod);
  return -prod;
}

// Exp of log_prob_fun
//static double prob_fun (double *p, size_t dim, void *_params) {
//  Params *params=(Params*)_params;
//  double ll=log_prob_fun(p,dim,params);
//  return exp(ll-params->likenorm);
//}

// Exp of log_prob_fun in acceptance region
static double tangent_region_fun (double *p, size_t dim, void *_params) {
  Params *params=(Params*)_params;
  double val=prob_fun(p,dim,params);

  if (val > params->cutoff) {
    return val;
  } else {
    return 0;
  }
}


double find_cutoff_alg(size_t dim,Params *par) {
  gsl_vector *w=gsl_vector_alloc(dim);
  gsl_vector_set_all (w,1.0);

  gsl_vector_div(w,par->vars);
  double a=1.0;

  gsl_blas_ddot(par->means,w,&a);
  double b=gsl_blas_dasum(w);
  double x=a/b;

  return  -prob_H0_fun(x,par);
}

double find_cutoff(size_t dim,Params *par) {
  int status;
  int iter = 0, max_iter = 100;
  const gsl_min_fminimizer_type *T;
  gsl_min_fminimizer *s;
  double m_expected = 0.0;
  double a = gsl_vector_min(par->means);
  double b = gsl_vector_max(par->means);
  double m = find_cutoff_alg(dim,par);

  gsl_function F;

  double pm = prob_H0_fun(m,par);

  printf("Find cutoff. f(%f)=%f Range: [%f,%f]\n",m,pm,a,b);

  F.function = &prob_H0_fun;
  F.params = (void*)par;

  T = gsl_min_fminimizer_brent;
  s = gsl_min_fminimizer_alloc (T);
  gsl_min_fminimizer_set (s, &F, m, a, b);

  printf ("using %s method\n",
	  gsl_min_fminimizer_name (s));

  printf ("%5s [%9s, %9s] %9s %10s %9s\n",
	  "iter", "lower", "upper", "min",
	  "err", "err(est)");

  printf ("%5d [%.7f, %.7f] %.7f %+.7f %.7f\n",
	  iter, a, b,
	  m, m - m_expected, b - a);

  do
    {
      iter++;
      status = gsl_min_fminimizer_iterate (s);

      m = gsl_min_fminimizer_x_minimum (s);
      a = gsl_min_fminimizer_x_lower (s);
      b = gsl_min_fminimizer_x_upper (s);

      status
	= gsl_min_test_interval (a, b, 0.001, 0.0);

      if (status == GSL_SUCCESS)
	printf ("Converged:\n");

      printf ("%5d [%.7f, %.7f] "
	      "%.7f %+.7f %.7f\n",
	      iter, a, b,
	      m, m - m_expected, b - a);
    }
  while (status == GSL_CONTINUE && iter < max_iter);

  gsl_min_fminimizer_free (s);

  return m;
}

void fbst(gsl_rng *r,
	  gsl_vector_uint *x,
	  gsl_vector_uint *sums,
	  double *_ev,double *_err,
	  FBSTConfig *config) {

  fprintf(stderr,"FBST\n");

  assert(x->size == sums->size);

  if (config->mc_config==NULL) {
    config->mc_config=MC_DEFAULTS;
  }

  size_t dim = x->size;

  fprintf(stderr,"dim = %zi\n",dim);

  Params *params=(Params*)malloc(sizeof(Params));

  size_t i;

  // a*log(Na)+b*log(Nb)-(a+b)*log(Na+Nb);

  unsigned int k=0;
  unsigned int NN=0;

  gsl_vector *means = gsl_vector_alloc(dim);
  gsl_vector *vars  = gsl_vector_alloc(dim);

  double delta=1.0/dim;

  for(i=0;i<dim;i++) {
    unsigned int xi=ELT(x,i);
    unsigned int Ni=ELT(sums,i);

    gsl_vector_set(means,i,gsl_sf_psi(xi+delta) -
		   gsl_sf_psi_int(Ni-xi-delta));
    gsl_vector_set(vars,i,sqrt(gsl_sf_psi_1(xi+delta) +
			       gsl_sf_psi_1(Ni+1.0-xi-delta)));
    k  += xi;
    NN += Ni;

    //cutoff1+=ELT(x,i)*log(ELT(sums,i));
  }

  // Verificar cutoff = NaN
  //fprintf(stderr,"cutoff1 = %.5g\n",cutoff1);

  params->k=k;
  params->means=means;
  params->dim=dim;
  params->vars=vars;

  printf("%lu %lu\n",params->dim,dim);

  double cutoff=find_cutoff_alg(dim,params);
  //double cutoff2=find_cutoff(dim,params);

  fprintf(stderr,"cutoff   = %.5g\n",cutoff);
  //fprintf(stderr,"cutoff2  = %.5g\n",cutoff2);

  params->cutoff=cutoff;


  /*double *phat=(double*)malloc(dim*sizeof(double));
  for(i=0;i<dim;i++) {
    // Add 1 to eliminet phat[i] = 0
    if (ELT(x,i) != 0) {
      phat[i]=ELT(x,i)*1.0/ELT(sums,i);
    } else {
      phat[i]=SMALL_P;
    }
    }*/


  //double likenorm = log_prob_fun(phat,dim,params);
  //params->likenorm = likenorm;

  //fprintf(stderr,"likenorm = %.5g\n",likenorm);

  //if (isnan(likenorm)) {
  //  likenorm = 0;
  //}



  gsl_monte_function G = { &prob_fun, dim, params };
  gsl_monte_function T = { &tangent_region_fun, dim, params };

  double g,g_err;
  double t,t_err;

  double *xl = (double*)malloc(dim*sizeof(double));
  double *xu = (double*)malloc(dim*sizeof(double));

  for (i=0;i<dim;i++) {
    xl[i]=gsl_vector_get(means,i)-6.0*gsl_vector_get(vars,i);
    xu[i]=gsl_vector_get(means,i)+6.0*gsl_vector_get(vars,i);
  }

#ifdef DUAL_INTEGRATE
  mc_dual_integrate(&G,&T,xl,xu,r,&g,&g_err,&t,&t_err,config->mc_config);
#else
  mc_integrate(&G,r,&g,&g_err,config->mc_config);

  fprintf(stderr,"null = %.5g\n",g);

  mc_integrate(&T,r,&t,&t_err,config->mc_config);
#endif


  free(xl);
  free(xu);


  *_err=t/g*sqrt(square(g_err/g)+square(t_err/t));
  double ev=1.0-t/g;

  if (ev < 0) {
    ev=0;
  }

  *_ev=ev;

  fprintf(stderr,"evidence = %.5g\n",*_ev);

  //free(phat);
  gsl_vector_free(means);
  gsl_vector_free(vars);
  free(params);
}
