#ifndef KEMP_CUTOFF_FUN_H
#define KEMP_CUTOFF_FUN_H

#include <gsl/gsl_matrix_double.h>

typedef struct {
  short max_k;
  double patch_region[2];

  gsl_matrix *coeffs;
} cutoff_fun_t;

double cutoff_function(cutoff_fun_t *par,short k,unsigned y,double *alpha);
cutoff_fun_t *read_cutoff_function(int argc,char **argv);

extern char *opt_pars_file;

#endif
